Require Import Lia List Nat.
Import ListNotations.

Lemma list_elt_exists : forall (A : Type) (l : list A) i,
    i < List.length l -> (exists a, List.nth_error l i = Some a).
Proof.
  intros A l i Hi.
  destruct nth_error eqn:Hnth.
  - now exists a.
  - destruct (nth_error_None l i) as [HNone _].
    specialize (HNone Hnth).
    lia.
Qed.

Section list_utils.
  Variable A B : Type.
  Variable a : A.
  Variable f : A -> B.
  Variable l lpre lpost : list A.
  Variable i : nat.

  Hypothesis Hl : l = lpre ++ a :: lpost.
  Hypothesis Hi : i = List.length lpre.

  Lemma i_lt_length_l : i < List.length l.
  Proof.
    rewrite Hl, app_length, Hi.
    simpl.
    lia.
  Qed.

  Lemma map_in_the_middle : List.nth_error (List.map f l) i = Some (f a).
  Proof.
    apply List.map_nth_error.
    now rewrite Hl, Hi, List.nth_error_app2, PeanoNat.Nat.sub_diag.
  Qed.

  Lemma list_is_composed :
    forall (l' : list A), List.length l = List.length l' ->
          (exists a' lpre' lpost', l' = lpre' ++ a' :: lpost' /\ i = List.length lpre').
  Proof.
    intros l' Hlength.

    pose proof i_lt_length_l as Hlength'.
    rewrite Hlength in Hlength'.

    pose proof (list_elt_exists _ _ _ Hlength') as Hle.
    destruct Hle as [a' Hle].

    pose proof (nth_error_split l' i Hle) as Hnes.
    destruct Hnes as [lpre' [lpost' [Hnes Hi']]].
    exists a', lpre', lpost'.
    split.
    - exact Hnes.
    - now apply eq_sym in Hi'.
  Qed.
End list_utils.

Lemma list_is_empty_or_composed :
  forall (A : Type) (l : list A),
    l = [] \/ (exists (a : A) (lpre lpost : list A) (i : nat), l = lpre ++ a :: lpost /\ i = List.length lpre).
Proof.
  intros A l.

  induction l.
  - now left.
  - right.
    destruct IHl as [IHl | IHl].
    + exists a, [], [], 0.
      now rewrite IHl.
    + destruct IHl as [a0 [lpre [lpost IHl]]].
      now exists a, [], l, 0.
Qed.

Lemma n_le_he_eq : forall (n m : nat), n <= m -> n >= m  -> n = m.
Proof.
  lia.
Qed.
