Require Import Bool Lia List Nat PeanoNat.
From StoreBuffer Require Import modular_definitions utils.
Import ListNotations.

Module StoreBufferSig <: Sig.
  Variant stage_sb :=
    | Sp
    | Lsu
    | Lu
    | Su
    | Sn.

  Definition first_stage := Sp.

  Scheme Equality for stage_sb.
  Definition stage := stage_sb.
  Definition stage_beq := stage_sb_beq.
  Definition stage_eq_dec := stage_sb_eq_dec.

  Lemma stage_beq_iff_eq : forall s s', stage_beq s s' = true <-> s = s'.
  Proof.
    intros s s'.
    now destruct s, s'.
  Qed.

  Corollary stage_beq_eq_true : forall s, stage_beq s s = true.
  Proof.
    intro s.
    now rewrite stage_beq_iff_eq.
  Qed.

  Corollary stage_nbeq_iff_neq : forall s s', stage_beq s s' = false <-> s <> s'.
  Proof.
    intros s s'.
    rewrite <- not_true_iff_false.
    apply not_iff_compat, stage_beq_iff_eq.
  Qed.

  (* Boolean stage comparison.  Sp < Lsu < (Lu, Su) < Sn. *)
  Definition leb x y :=
    match x, y with
    | Sp, _ => true
    | _, Sn => true
    | Sn, _ => false
    | Lsu, Sp => false
    | Lsu, _ => true
    | Lu, Lu => true
    | Lu, _ => false
    | Su, Su => true
    | Su, _ => false
    end.

  Definition can_have_lat st :=
    match st with
    | Sp | Su | Sn => false
    | _ => true
    end.

  Lemma stage_beq_implies_leb : forall st st', stage_beq st st' = true -> leb st st' = true.
  Proof.
    intros st st'.
    now destruct st, st'.
  Qed.

  Definition lats := (stage_sb * nat)%type.

  Variant opcode_sb :=
    | Load (lat : list lats) (id : nat)
    | Store (lat : list lats) (id : nat).

  Definition opcode := opcode_sb.

  Definition lat opc :=
    match opc with
    | Load lat _ | Store lat _ => lat
    end.

  Definition idx opc :=
    match opc with
    | Load _ id | Store _ id => id
    end.
End StoreBufferSig.

Module StoreBuffer := Pipeline StoreBufferSig.
Export StoreBuffer.

Definition nstg s opc :=
  match s, opc with
  | Sp, _ => Lsu
  | Lsu, Load _ _ => Lu
  | Lsu, Store _ _ => Su
  | _, _ => Sn
  end.

Definition sbEmpty := sbStateT_beq Empty.

(* ready, free and cycle functions. *)
Definition ready next_id sbState suNotFree (elt : instr_kind) :=
  match elt with
  | (opc, (Sp, 0)) => idx opc =? next_id
  | (_, (Su, _)) => negb (sbStateT_beq sbState Full)
  | (Load _ _, (Lsu, 0)) => sbEmpty sbState && (negb suNotFree)
  | (_, (_, lat)) => lat =? 0
  end.

Definition luSuFree next_id sbState trace suNotFree stg :=
  let instr := get_instr_in_stage trace stg in
  match instr with
  | Some elt => ready next_id sbState suNotFree elt (* Sn is always free *)
  | None => true
  end.

Definition lsuFree next_id sbState trace suNotFree :=
  let instr := get_instr_in_stage trace Lsu in
  match instr with
  | Some (opcode, (stg, lat)) => ready next_id sbState suNotFree (opcode, (stg, lat)) &&
                                 luSuFree next_id sbState trace suNotFree (nstg stg opcode)
  | None => true
  end.

Definition free next_id sbState trace suNotFree (elt : instr_kind) :=
  let (opc, state) := elt in
  let (st, _) := state in
  match nstg st opc with
  | Lu => luSuFree next_id sbState trace suNotFree Lu
  | Su => luSuFree next_id sbState trace suNotFree Su
  | Lsu => lsuFree next_id sbState trace suNotFree
  | _ => true
  end.

Definition get_latency opc st :=
  if can_have_lat st then
    let lat := List.find (fun stl =>
                            match stl with
                            | (stl, _) => stage_beq st stl
                            end) (lat opc) in
    match lat with
    | None => 0
    | Some (_, n) => n
    end
  else
    0.

Definition cycle_elt next_id busTaken sbState trace suNotFree elt :=
  match elt with
  | (opc, (st, S n)) =>
      if (negb (stage_beq Lu st)) || busTaken then
        (opc, (st, n))
      else
        elt
  | (opc, (st, 0)) =>
      if ready next_id sbState suNotFree elt && free next_id sbState trace suNotFree elt then
        let nstg := nstg st opc in
        (opc, (nstg, get_latency opc nstg))
      else
        elt
  end.

Definition suNotFree := List.existsb (is_in_stage Su).
Definition luNotFree := List.existsb (is_in_stage Lu).

Definition cycle sbState busTaken trace :=
  List.map (cycle_elt (next_id trace) busTaken sbState trace (suNotFree trace)) trace.

Definition legal_sb_transitions st sbState sbState' :=
  match st, sbState with
  | Lsu, Empty => sbState' = Empty
  | Su, Empty => sbState' = Empty
  | Su, NotEmpty => sbState' = NotEmpty \/ sbState' = Empty
  | _, _ => sbState' = Full \/ sbState' = NotEmpty \/ sbState' = Empty
  end.

Lemma opc_match_case_identical : forall (A : Type) (a : A) opc,
    match opc with | Load _ _ | _ => a end = a.
Proof.
  intros A a opc.
  now destruct opc.
Qed.

Section instr_progress_generalization.
  Variable opc : opcode.
  Variable st st' : stage.
  Variable n : nat.

  Lemma next_stage_is_higher :
    st' = nstg st opc ->
    compare_two (Some (opc, (st, 0)))
      (Some (opc, (st', get_latency opc st'))) = true.
  Proof.
    intro Hst'.
    rewrite Hst'.
    destruct st;
      try reflexivity;
      now destruct opc.
  Qed.

  Lemma state_leb_n0 : state_leb (st, 0) (st', S n) = true ->
                       stage_beq st st' = false.
  Proof.
    simpl.
    now destruct stage_beq.
  Qed.

  Lemma state_leb_diff : (opc, (st, 0)) <> (opc, (st', 0)) ->
                         stage_beq st st' = false.
  Proof.
    intro Hed.
    rewrite stage_nbeq_iff_neq.
    intro Hst.
    destruct Hed.
    now rewrite Hst.
  Qed.

  Lemma leb_remains : (st' = Lu -> exists lat id, opc = Load lat id) ->
                      (st' = Su -> exists lat id, opc = Store lat id) ->
                      stage_beq st st' = false -> leb st st' = true ->
                      leb (nstg st opc) st' = true.
  Proof.
    intros Hlu Hsu Hst Hleb.

    destruct st;
      [| destruct opc |..];
      destruct st';
      auto.
    - now destruct Hsu as [lat [id' Hopc]].
    - now destruct Hlu as [lat [id' Hopc]].
  Qed.

  Lemma leb_far_remains : stage_beq st st' = false -> leb st st' = true ->
                          leb (nstg st opc) (nstg st' opc) = true.
  Proof.
    intros Hst Hleb.
    destruct st, st';
      auto;
      now destruct opc.
  Qed.

  Lemma stage_beq_nstg_nstg_eq : stage_beq st st' = false -> leb st st' = true ->
                                 stage_beq (nstg st opc) (nstg st' opc) = true ->
                                 st' = Sn.
  Proof.
    destruct st, st';
      try discriminate;
      now destruct opc.
  Qed.

  Lemma stage_beq_nstg_sn : leb st st' = true ->
                            stage_beq st (nstg st' opc) = true -> st' = Sn.
  Proof.
    destruct st, st';
      try discriminate;
      now destruct opc.
  Qed.

  Lemma leb_nstg_right : leb st st' = true -> leb st (nstg st' opc) = true.
  Proof.
    destruct st, st';
      try discriminate;
      now destruct opc.
  Qed.

  Lemma right_progresses_state_leb :
    (if stage_beq st st' then true else leb st st') = true ->
    (if stage_beq st (nstg st' opc) then
       get_latency opc (nstg st' opc) <=? n
     else
       leb st (nstg st' opc)) = true.
  Proof.
    intro Hconstr.

    destruct (stage_beq _ st') eqn:Hsb;
      [apply stage_beq_implies_leb in Hsb |];
      destruct (stage_beq _ (nstg st' _)) eqn:Hsb'.
    - now rewrite stage_beq_nstg_sn.
    - now apply leb_nstg_right.
    - now rewrite stage_beq_nstg_sn.
    - now apply leb_nstg_right.
  Qed.
End instr_progress_generalization.

Section monotonicity.
  Variable opc : opcode.
  Variable d : instr_kind.
  Variable st st' : stage.
  Variable t tpre tpost t' tpre' tpost' tc tc' : trace_kind.
  Variable busTaken busTaken' : bool.
  Variable sbState sbState' : sbStateT.

  Hypothesis Hleb : pipeline_leb t t' = true.
  Hypothesis Hl : pipeline_leb tpre tpre' = true.
  Hypothesis Hl' : pipeline_leb tpost tpost' = true.
  Hypothesis Htc : tc = cycle sbState busTaken t.
  Hypothesis Htc' : tc' = cycle sbState' busTaken' t'.
  Hypothesis Hlsbt : legal_sb_transitions st sbState sbState'.
  Hypothesis HnId : next_id t <= next_id t'.

  Hypothesis HnIdLow : forall t (e : instr_kind),
  match e with
  | (opc, (st, _)) => idx opc < next_id t <-> st <> Sp
  end.

  Hypothesis HnIdHigh : forall t (e : instr_kind),
  match e with
  | (opc, (st, _)) => idx opc >= next_id t <-> st = Sp
  end.

  Hypothesis Hlu : forall st opc, st = Lu -> exists lat id, opc = Load lat id.
  Hypothesis Hsu : forall st opc, st = Su -> exists lat id, opc = Store lat id.

  (* Long hypotheses related to stage occupation. *)
  Hypothesis HfreeLsuPersists :
    free (next_id t) sbState t (suNotFree t) (opc, (Sp, 0)) = true ->
    free (next_id t') sbState' t' (suNotFree t') (opc, (Sp, 0)) = true. (* TODO prove *)

  Hypothesis HfreeFromLsuPersists :
    free (next_id t) sbState t (suNotFree t) (opc, (Lsu, 0)) = true ->
    free (next_id t') sbState' t' (suNotFree t') (opc, (Lsu, 0)) = true. (* TODO prove *)

  Hypothesis HsuUsed : st = Lsu -> suNotFree t = false -> suNotFree t' = false. (* TODO prove *)
  Hypothesis HluUsed : st = Lsu -> luNotFree t = false -> luNotFree t' = false. (* TODO prove *)
  Hypothesis HbusTaken : forall st n busTaken, st = Lu -> n <> 0 -> busTaken = true.

  Hypothesis HvalidLat : forall (e : instr_kind),
  match e with
  | (opc, (st, n)) => n <= get_latency opc st
  end.

  Section simple_monotonicity.
    Variable e e' : instr_kind.
    Variable i : nat.

    Hypothesis Ht : t = tpre ++ e :: tpost.
    Hypothesis Ht' : t' = tpre' ++ e' :: tpost'.
    Hypothesis Hi : i = List.length tpre.
    Hypothesis Hi' : i = List.length tpre'.

    Ltac work_on_e He' :=
      rewrite Htc, Htc', Ht;
        match type of He' with
        | e = e' =>
            rewrite Ht', <- He' in HfreeLsuPersists, HfreeFromLsuPersists, HnId |- *
        | e <> e' =>
            rewrite Ht'
        end;
        unfold cycle;
        rewrite (map_in_the_middle _ _ e _ _ tpre tpost _ eq_refl Hi),
          (map_in_the_middle _ _ _ _ _ tpre' tpost' _ eq_refl Hi');
        unfold cycle_elt;
        rewrite <- Ht;
        match type of He' with
        | e = e' =>
            subst e
        | e <> e' =>
            rewrite <- Ht';
              subst e;
              subst e'
        end.

    Ltac stage_opc Hyp :=
      destruct Hyp as [lat [id Hopc]]; [
          reflexivity |
          (discriminate Hopc ||
             rewrite Hopc;
             auto)
        ].

    Lemma unmoved_no_lat_can_advance :
      e = (opc, (st, 0)) -> e = e' -> forall n,
      List.nth_error tc i = Some (opc, (nstg st opc, n)) ->
      List.nth_error tc' i = Some (opc, (nstg st opc, n)).
    Proof.
      intros He He' n.
      specialize (Hlu st opc).
      specialize (Hsu st opc).
      specialize (HnIdLow t' e').
      specialize (HnIdHigh t' e').

      rewrite Ht', <- He' in HnIdLow, HnIdHigh, HsuUsed, HluUsed.

      (* Delve onto e, then handle stages on a case-by-case basis. *)
      work_on_e He'.
      destruct st.

      (* Sp.  The proof does not rely on the kind of instruction. *)
      - destruct free in HfreeLsuPersists |- *.
        + (* LSU is free in t. *)
          setoid_rewrite (HfreeLsuPersists eq_refl).
          unfold ready.
          repeat rewrite opc_match_case_identical.

          destruct (_ =? _) eqn:Hidx; [| discriminate].
          rewrite Nat.eqb_eq in Hidx.
          rewrite <- Hidx in HnId.
          destruct HnIdHigh as [_ HnIdHigh'].
          now rewrite <- (n_le_he_eq _ _ HnId (HnIdHigh' eq_refl)), Nat.eqb_refl.

        + (* LSU is not free in t: this is irrelevant. *)
          rewrite andb_false_r.
          discriminate.

      (* Lsu *)
      - destruct free, opc.
        (* Cases where e progressed in t. *)
        + (* Loads *)
          simpl in Hlsbt, HfreeFromLsuPersists |- *.
          destruct sbState; [| discriminate..].
          subst sbState'.
          rewrite (HfreeFromLsuPersists eq_refl).
          destruct (suNotFree t); [discriminate |].
          now rewrite HsuUsed.

        + (* Stores *)
          now rewrite <- HfreeFromLsuPersists.

        (* Cases where e did not progress in t. *)
        (* Irrelevant, exclude them. *)
        + rewrite andb_false_r.
          discriminate.
        + discriminate.

      (* Lu: instructions without latency (this is the case here) move to Sn. *)
      - stage_opc Hlu.

      (* Su: if there is space in the store buffer, move to Sn, otherwise, remain
       in Su.  Eliminate cases where sbState/sbState' are inconsistent. *)
      - stage_opc Hsu.
        simpl in Hlsbt.
        destruct sbState; [..| discriminate].
        + now rewrite Hlsbt.
        + destruct Hlsbt as [HsbState' | HsbState'];
            now rewrite HsbState'.

      (* Sn: instructions remains in Sn. *)
      - auto.
    Qed.

    Lemma unmoved_is_monotonic :
      forall n, e = (opc, (st, n)) -> e = e' ->
                compare_two (List.nth_error tc i) (List.nth_error tc' i) = true.
    Proof.
      intros n He He'.
      specialize (HbusTaken st n).

      (* Extract e. *)
      pose proof unmoved_no_lat_can_advance as Huca.
      revert Huca.
      work_on_e He'.
      intro Huca.

      destruct n.
      - (* Case where an instruction has a latency of 0. *)
        destruct (_ && _).
        + (* If e (in t) can progress. *)
          (* e' (in t') can progress to.  We have a lemma for this. *)
          rewrite (Huca eq_refl eq_refl (get_latency _ _) eq_refl).
          now apply compare_two_eq_true.
        + (* If e (in t) cannot progress. *)
          destruct (_ && _).
          * (* If e (in t') can progress. cycle_elt e t' > cycle_elt e t.  This
             * is trivial, but each case must be analysed separately. *)
            now apply next_stage_is_higher.
          * (* If e (in t') cannot progress. cycle_elt e t' = cycle e  t. This
             * is also trivial. *)
            now apply compare_two_eq_true.

      - destruct st;
          try (simpl;
                 now rewrite Nat.leb_refl).
        rewrite (HbusTaken busTaken), (HbusTaken busTaken');
          auto.
    Qed.

    Lemma moved_lat_left_is_monotonic :
      forall n, e = (opc, (st, n)) -> e' = (opc, (st', 0)) -> e <> e' ->
           state_leb (st, n) (st', 0) = true ->
           compare_two (List.nth_error tc i) (List.nth_error tc' i) = true.
    Proof.
      intros n He He' Hed Hsl.

      (* The idea of this proof is as follows:
       * If we have two instructions, e and e', with e' more advanced than e,
       * and e''s latency is equal to 0, e will be, in the worst case, able to
       * obtain the same progress than e', but not overtake it.
       *)

      (* Extract (cycle_elt ... e ). *)
      work_on_e Hed.

      (* Case-by-case analysis on e's latency, then on the conditions that
       * decide whether or not an instruction can progress. *)
      destruct n;
        repeat match goal with
          | [ _ : _ |- context [ if ?X then _ else _ ] ] => destruct X
          end;
        (* In some cases, e and e' won't progress (or at least, remain in the
         * same stage for e), so compare_two ... =
         * (if stage_beq st st' then true else leb st st') (or some
         * complexified variant on this).  This is the same as Hsl, so try to
         * apply it preemptively. *)
        try assumption;
        simpl in Hsl |- *.

      (* n = 0. *)
      (* Since e <> e', st < st'.  Interesting cases are where e can progress,
       * and e cannot while e' can.  If e and e' cannot progress, st and st'
       * remain the same, and it is the same a proving Hsl.  These cases were
       * handled before. *)
      - (* e and e' progressed. *)
        rewrite (state_leb_diff opc st st' Hed) in Hsl.
        apply state_leb_diff in Hed.
        destruct (stage_beq (nstg _ _) _) eqn:Hsb.
        + (* nstg st = nstg st'.  This means that st = st' = Sn, and their
           * latency is equal to 0. *)
          now rewrite (stage_beq_nstg_nstg_eq opc st st').
        + (* nstg st <> nstg st' -> nstg st < nstg st'. *)
          (* We have a lemma for this. *)
          now apply leb_far_remains.
      - (* e progressed, e' did not. *)
        rewrite (state_leb_diff opc st st' Hed) in Hsl.
        apply state_leb_diff in Hed.
        destruct (stage_beq (nstg _ _) _).
        + (* nstg st = st'.  Trivial. *)
          reflexivity.
        + (* nstg st <> st' -> nstg st < st'. *)
          (* We have a lemma for this. *)
          now apply (leb_remains opc st st' (Hlu _ _) (Hsu _ _)).
      - (* e cannot progress, but e' can. *)
        now apply right_progresses_state_leb.

      (* n <> 0. *)
      (* e will remain st, but e' may progress to the next stage.  If it can
       * not, this is the same as proving Hsl (in the hypothesis), and these
       * cases were already handled earlier.  If it can, we have to prove that
       * e's state is still lower than e'.  We have a lemma for this. *)
      - (* Case where e did not progress (ie. st = Lu and busTaken = false). *)
        (* cycle_elt ... e = (opc, (st, S n)) *)
        now apply right_progresses_state_leb.
      - (* Case where e did progress.  cycle_elt ... e = (opc, (st, n)). *)
        now apply right_progresses_state_leb.
    Qed.

    Lemma moved_lat_is_monotonic :
      forall n n', e = (opc, (st, n)) -> e' = (opc, (st', n')) ->
              state_leb (st, n) (st', n') = true -> e <> e' ->
              compare_two (List.nth_error tc i) (List.nth_error tc' i) = true.
    Proof.
      intros n n' He He' Hsl Hed.
      clear HfreeFromLsuPersists HfreeLsuPersists Hlsbt.
      specialize (HvalidLat e').
      simpl in Hsl.

      (* Case by case analysis on e''s latency. *)
      destruct n';
        (* n' = 0, we have a lemma for this. *)
        [now apply (moved_lat_left_is_monotonic n) |];
        work_on_e Hed;
        (* Case by case analysis on e's latency. *)
        destruct n.

      - (* n = 0, n' <> 0. *)
        (* So, e is in another stage than e'.
         * In the worst case, e will be in the same stage as e' in the next
         * cycle, and e' will remain in the same stage.  This will give:
         * compare_two (opc, (st', ?N)) (opc, (st', n' - 1)) = true.
         * As ?N = get_latency opc st, and n' <= get_latency opc st,
         * n' - 1 < ?N, hence the proposition above is true.
         *)
        pose proof (state_leb_n0 _ _ _ Hsl) as Hst.
        specialize (Hlu st' opc).
        specialize (Hsu st' opc).

        rewrite Hst in Hsl.
        unfold compare_two, state_leb.

        (* e may go to the next stage, e''s latency may be reduced.  Check all
         * these situations. *)
        destruct (_ && _), (_ || _);
          (* e go to the next stage: a finer analysis is required. *)
          (destruct (stage_beq (nstg _ _) _) eqn:Hsb
          (* e does not go to the next stage: st = st' and st < st'
           * still hold. *)
          (* compare_two ... = if st =? st' then ... else st < st'. *)
            || now rewrite Hst);
          (* e went to the next stage. *)
          match goal with
          | [ _ : _ |- leb _ _ = true ] =>
              (* Cases where nstg st <> st'. *)
              (* We must prove that nstg st < st'.  We have a lemma for this. *)
              now apply leb_remains
          | _ =>
              rewrite stage_beq_iff_eq in Hsb;
                rewrite Hsb, Nat.leb_le
          end.

        (* Cases where nstg st = st'. *)
        + (* Case where e''s latency decreased. *)
          (* We have to prove that n' < get_latency opc st',
           * and we know that S n' < get_latency opc st. *)
          lia.
        + (* Case where e''s latency did not decrease (ie. st' = Lu). *)
          (* We have to prove that S n' < get_latency opc st,
           * while it is already in the hypothesis. *)
          exact HvalidLat.

      - (* n and n' <> 0. *)
        destruct (stage_eq_dec st st') as [Heq | Hdiff].
        + (* st = st'. *)
          rewrite Heq in Hsl |- *.
          specialize (HbusTaken st').
          destruct st';
            try auto.
          rewrite (HbusTaken (S n) busTaken), (HbusTaken (S n') busTaken');
            auto.

        + (* st <> st'. *)
          (* Both instructions will remain in their respective stages.
           * (Their latency may or may not decrease, but that's not
           * important.) *)
          unfold compare_two, state_leb.
          rewrite <- stage_nbeq_iff_neq in Hdiff.
          rewrite Hdiff in Hsl.
          destruct (_ || _), (_ || _);
            now rewrite Hdiff.
    Qed.

    Theorem is_monotonic :
      forall n n', e = (opc, (st, n)) -> e' = (opc, (st', n')) ->
                   state_leb (st, n) (st', n') = true ->
                   compare_two (List.nth_error tc i) (List.nth_error tc' i) = true.
    Proof.
      intros n n' He He' Hsl.

      (* e = e' or e <> e'. *)
      compare e e'.
      - (* If e = e', we have a lemma for this. *)
        now apply (unmoved_is_monotonic n).
      - (* If e <> e', we have a lemma for this. *)
        now apply (moved_lat_is_monotonic n n').

      (* state and opcode are comparable. *)
      - repeat decide equality.
      - repeat decide equality.
    Qed.
  End simple_monotonicity.

  Let tc_and_tc' :=
        tc_tc'_length_eq (cycle_elt (next_id t) busTaken sbState t (suNotFree t))
          (cycle_elt (next_id t') busTaken' sbState' t' (suNotFree t'))
          t t' tc tc' Hleb Htc Htc'.

  Variable e e' : instr_kind.
  Variable n n' : nat.

  Hypothesis Ht : t = tpre ++ e :: tpost.
  Hypothesis Ht' : t' = tpre' ++ e' :: tpost'.
  Hypothesis Hi : forall i, i = List.length tpre.
  Hypothesis Hi' : forall i, i = List.length tpre'.

  Hypothesis He : e = (opc, (st, n)).
  Hypothesis He' : e' = (opc, (st', n')).
  Hypothesis Hsl : state_leb (st, n) (st', n') = true.

  Let is_monotonic' ni :=
        is_monotonic e e' ni Ht Ht' (Hi ni) (Hi' ni) n n' He He' Hsl.

  Let cycled_instrs_are_leb' :=
        cycled_instrs_are_leb d tc tc' is_monotonic' tc_and_tc'.

  Theorem is_fully_monotonic : pipeline_leb tc tc' = true.
  Proof.
    now apply is_pipeline_fully_monotonic.
  Qed.
End monotonicity.
