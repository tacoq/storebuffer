Require Import Bool Lia List Nat PeanoNat.
From StoreBuffer Require Import utils modular_definitions.
Import ListNotations.

Module FiveStageSig <: Sig.
  Variant stage_fs :=
    | Pre
    | Ex
    | Mem
    | Wb
    | Post.

  Definition first_stage := Pre.

  Scheme Equality for stage_fs.
  Definition stage := stage_fs.
  Definition stage_beq := stage_fs_beq.
  Definition stage_eq_dec := stage_fs_eq_dec.

  Lemma stage_beq_iff_eq : forall s s', stage_beq s s' = true <-> s = s'.
  Proof.
    now destruct s, s'.
  Qed.

  Corollary stage_beq_eq_true : forall s, stage_beq s s = true.
  Proof.
    intro s.
    now rewrite stage_beq_iff_eq.
  Qed.

  Corollary stage_nbeq_iff_neq : forall s s', stage_beq s s' = false <-> s <> s'.
  Proof.
    intros s s'.
    rewrite <- not_true_iff_false.
    apply not_iff_compat, stage_beq_iff_eq.
  Qed.

  (* Boolean stage comparison.  Pre < Ex < Mem < Wb < Post. *)
  Definition leb x y :=
    match x, y with
    | Pre, _ => true
    | Ex, Pre => false
    | Ex, _ => true
    | Mem, Pre => false
    | Mem, Ex => false
    | Mem, _ => true
    | Wb, Wb => true
    | Wb, Post => true
    | Wb, _ => false
    | Post, Post => true
    | Post, _ => false
    end.

  Definition can_have_lat st :=
    match st with
    | Pre | Post => false
    | _ => true
    end.

  Lemma stage_beq_implies_leb : forall st st',
      stage_beq st st' = true -> leb st st' = true.
  Proof.
    now destruct st, st'.
  Qed.

  Definition lats := (stage_fs * nat)%type.

  Variant opcode_fs :=
    | Load (lat : list lats) (id : nat)
    | Store (lat : list lats) (id : nat)
    | Other (lat : list lats) (id : nat).

  Definition opcode := opcode_fs.

  Definition lat opc :=
    match opc with
    | Load lat  _ | Store lat _ | Other lat _ => lat
    end.

  Definition idx opc :=
    match opc with
    | Load _ idx | Store _ idx | Other _ idx => idx
    end.
End FiveStageSig.

Module FiveStage := Pipeline FiveStageSig.
Export FiveStage.

Definition nstg s :=
  match s with
  | Pre => Ex
  | Ex => Mem
  | Mem => Wb
  | _ => Post
  end.

Definition busFree := sbStateT_beq Empty.

(* ready, free, and cycle functions. *)
Definition ready next_id sbState memNotFree (elt : instr_kind) :=
  match elt with
  | (_, (_, S _)) => false
  | (Store _ _, (Mem, 0)) => negb (sbStateT_beq Full sbState)
  | (Load _ _, (Ex, 0)) => busFree sbState && (negb memNotFree)
  | (opc, (Pre, 0)) => idx opc =? next_id
  | _ => true
  end.

Definition memFree next_id sbState trace memNotFree :=
  let instr := get_instr_in_stage trace Mem in
  match instr with
  | Some elt => ready next_id sbState memNotFree elt
  | None => true
  end.

Definition exFree next_id sbState trace memNotFree :=
  let instr := get_instr_in_stage trace Ex in
  match instr with
  | Some elt => ready next_id sbState memNotFree elt && memFree next_id sbState trace memNotFree
  | None => true
  end.

Definition free next_id sbState trace memNotFree (elt : instr_kind) :=
  let (opc, state) := elt in
  let (st, _) := state in
  match nstg st with
  | Mem => memFree next_id sbState trace memNotFree
  | Ex => exFree next_id sbState trace memNotFree
  | _ => true
  end.

Definition get_latency opc st :=
  if can_have_lat st then
    let lat := List.find (fun stl =>
                            match stl with
                            | (stl, _) => stage_beq st stl
                            end) (lat opc) in
    match lat with
    | None => 0
    | Some (_, n) => n
    end
  else
    0.

Definition cycle_elt next_id sbState trace memNotFree elt :=
  match elt with
  | (opc, (st, S n)) => (opc, (st, n))
  | (opc, (st, 0)) =>
      if ready next_id sbState memNotFree elt && free next_id sbState trace memNotFree elt then
        let nstg := nstg st in
        (opc, (nstg, get_latency opc nstg))
      else
        elt
  end.

Definition memNotFree := List.existsb (is_in_stage Mem).

Definition cycle sbState trace :=
  List.map (cycle_elt (next_id trace) sbState trace (memNotFree trace)) trace.

Definition legal_sb_transitions st sbState sbState' :=
  match st, sbState with
  | Mem, NotEmpty => sbState' = NotEmpty \/ sbState' = Empty
  | Ex, Empty => sbState' = Empty
  | Mem, Empty => sbState' = Empty
  | _, _ => sbState' = Full \/ sbState' = NotEmpty \/ sbState' = Empty
  end.

Lemma opc_match_case_identical : forall (A : Type) (a : A) opc,
    match opc with | Load _ _ | _ => a end = a.
Proof.
  intros A a opc.
  now destruct opc.
Qed.

Section instr_progress_generalization.
  Variable opc : opcode.
  Variable st st' : stage.
  Variable n : nat.

  Lemma next_stage_is_higher :
    st' = nstg st ->
    compare_two (Some (opc, (st, 0)))
      (Some (opc, (st', get_latency opc st'))) = true.
  Proof.
    intros Hst'.
    rewrite Hst'.
    now destruct st;
      [..| destruct opc].
  Qed.

  Lemma state_leb_n0 : state_leb (st, 0) (st', S n) = true ->
                       stage_beq st st' = false.
  Proof.
    simpl.
    now destruct stage_beq.
  Qed.

  Lemma state_leb_diff : (opc, (st, 0)) <> (opc, (st', 0)) ->
                         stage_beq st st' = false.
  Proof.
    intro Hed.
    rewrite stage_nbeq_iff_neq.
    intro Hst.
    destruct Hed.
    now rewrite Hst.
  Qed.

  Lemma leb_remains : stage_beq st st' = false -> leb st st' = true ->
                      leb (nstg st) st' = true.
  Proof.
    now destruct st, st'.
  Qed.

  Lemma leb_far_remains : stage_beq st st' = false -> leb st st' = true ->
                          leb (nstg st) (nstg st') = true.
  Proof.
    now destruct st, st'.
  Qed.

  Lemma stage_beq_nstg_nstg_eq : stage_beq st st' = false -> leb st st' = true ->
                                 stage_beq (nstg st) (nstg st') = true -> st' = Post.
  Proof.
    now destruct st, st'.
  Qed.

  Lemma stage_beq_nstg_post : leb st st' = true ->
                              stage_beq st (nstg st') = true -> st' = Post.
  Proof.
    now destruct st, st'.
  Qed.

  Lemma leb_nstg_right : leb st st' = true -> leb st (nstg st') = true.
  Proof.
    now destruct st, st'.
  Qed.

  Lemma right_progresses_state_leb :
    (if stage_beq st st' then true else leb st st') = true ->
    (if stage_beq st (nstg st') then
       get_latency opc (nstg st') <=? n
     else
       leb st (nstg st')) = true.
  Proof.
    intros Hconstr.

    destruct (stage_beq _ st') eqn:Hsb;
      [apply stage_beq_implies_leb in Hsb |];
      destruct (stage_beq _ (nstg _)) eqn:Hsb'.
    - now rewrite stage_beq_nstg_post.
    - now apply leb_nstg_right.
    - now rewrite stage_beq_nstg_post.
    - now apply leb_nstg_right.
  Qed.
End instr_progress_generalization.

Section monotonicity.
  Variable opc : opcode.
  Variable d : instr_kind.
  Variable st st' : stage.
  Variable t tpre tpost t' tpre' tpost' tc tc' : trace_kind.
  Variable sbState sbState' : sbStateT.

  Hypothesis Hleb : pipeline_leb t t' = true.
  Hypothesis Hl : pipeline_leb tpre tpre' = true.
  Hypothesis Hl' : pipeline_leb tpost tpost' = true.
  Hypothesis Htc : tc = cycle sbState t.
  Hypothesis Htc' : tc' = cycle sbState' t'.
  Hypothesis Hlsbt : legal_sb_transitions st sbState sbState'.
  Hypothesis HnId : next_id t <= next_id t'.

  Hypothesis HnIdLow : forall t (e : instr_kind),
      match e with
      | (opc, (st, _)) => idx opc < next_id t <-> st <> Pre
      end.
  Hypothesis HnIdHigh : forall t (e : instr_kind),
      match e with
      | (opc, (st, _)) => idx opc >= next_id t <-> st = Pre
      end.

  Hypothesis HfreePersists :
    forall (st : stage), free (next_id t) sbState t (memNotFree t) (opc, (st, 0)) = true ->
                    free (next_id t') sbState' t' (memNotFree t') (opc, (st, 0)) = true. (* TODO prove *)

  Hypothesis HmemUsed : st = Ex -> memNotFree t = false -> memNotFree t' = false. (* TODO prove *)

  Hypothesis HvalidLat : forall (e : instr_kind),
      match e with
      | (opc, (st, n)) => n <= get_latency opc st
      end.

  Section simple_monotonicity.
    Variable e e' : instr_kind.
    Variable i : nat.

    Hypothesis Ht : t = tpre ++ e :: tpost.
    Hypothesis Ht' : t' = tpre' ++ e' :: tpost'.
    Hypothesis Hi : i = List.length tpre.
    Hypothesis Hi' : i = List.length tpre'.

    Ltac work_on_e He' :=
      rewrite Htc, Htc', Ht;
        match type of He' with
        | e = e' =>
            rewrite Ht', <- He' in HfreePersists, HnId |- *
        | e <> e' =>
            rewrite Ht'
        end;
        unfold cycle;
        rewrite (map_in_the_middle _ _ e _ _ tpre tpost _ eq_refl Hi),
          (map_in_the_middle _ _ _ _ _ tpre' tpost' _ eq_refl Hi');
        unfold cycle_elt;
        rewrite <- Ht;
        match type of He' with
        | e = e' =>
              subst e
        | e <> e' =>
            rewrite <- Ht';
              subst e;
              subst e'
        end.

    Lemma unmoved_no_lat_can_advance :
      e = (opc, (st, 0)) -> e = e' -> forall n,
      List.nth_error tc i = Some (opc, (nstg st, n)) ->
      List.nth_error tc' i = Some (opc, (nstg st, n)).
    Proof.
      intros He He' n.
      specialize (HfreePersists st).
      (* specialize (HbusTaken e). *)
      (* specialize (HbusFree st). *)
      specialize (HnIdLow t' e').
      specialize (HnIdHigh t' e').

      rewrite Ht', <- He' in HnIdLow, HnIdHigh, HmemUsed.

      (* Delve onto e, then handle stages on a case-by-case basis. *)
      work_on_e He'.
      destruct st;
        (* Solve Wb and Post automatically. *)
        [..| auto | auto].

      (* Pre. *)
      - destruct free.
        + destruct free;
            [| now discriminate HfreePersists].

          unfold ready.
          repeat rewrite opc_match_case_identical.

          destruct (_ =? _) eqn:Hidx;
            [| discriminate].
          rewrite Nat.eqb_eq in Hidx.
          rewrite <- Hidx in HnId.
          destruct HnIdHigh as [_ HnIdHigh'].
          now rewrite <- (n_le_he_eq _ _ HnId (HnIdHigh' eq_refl)), Nat.eqb_refl.

        + rewrite andb_false_r.
          discriminate.

      (* Ex. *)
      - destruct free.
        + (* Case where e can progress in t. *)
          rewrite (HfreePersists eq_refl).
          destruct opc;
            (* Stores and others are trivial. *)
            [| auto..].

          (* Loads *)
          destruct sbState; [| discriminate..].
          destruct (memNotFree t); [discriminate |].
          now rewrite Hlsbt, HmemUsed.

        + (* Case where e cannot progress in t. *)
          (* This is irrelevant, eliminate it. *)
          rewrite andb_false_r.
          discriminate.

      (* Mem. *)
      - now destruct opc; [
            (* Loads and stores are trivial; stores are trickier. *)
          | destruct sbState, sbState', Hlsbt;
              try discriminate |].
    Qed.

    Lemma unmoved_is_monotonic :
      forall n, e = (opc, (st, n)) -> e = e' ->
                compare_two (List.nth_error tc i) (List.nth_error tc' i) = true.
    Proof.
      intros n He He'.

      (* Extract e. *)
      pose proof unmoved_no_lat_can_advance as Huca.
      revert Huca.
      work_on_e He'.
      intro Huca.

      destruct n.
      - (* Case where an instruction has a latency of 0. *)
        destruct (_ && _).
        + (* If e (in t) can progress. *)
          (* e' (in t') can progress to.  We have a lemma for this. *)
          rewrite (Huca eq_refl eq_refl (get_latency opc (nstg st)) eq_refl).
          now apply compare_two_eq_true.

        + (* If e (in t) cannot progress. *)
          destruct (_ && _).
          * now apply next_stage_is_higher.
          * now apply compare_two_eq_true.

      - (* Case where an instruction has a latency higher than 0. *)
        (* cycle_elt ... e = cycle_elt ... e'.  Then, compare_two ... = true. *)
        now apply compare_two_eq_true.
    Qed.

    Lemma moved_lat_left_is_monotonic :
      forall n, e = (opc, (st, n)) -> e' = (opc, (st', 0)) -> e <> e' ->
           state_leb (st, n) (st', 0) = true ->
           compare_two (List.nth_error tc i) (List.nth_error tc' i) = true.
    Proof.
      intros n He He' Hed Hsl.
      clear HfreePersists Hlsbt.

      work_on_e Hed.

      destruct n;
        repeat match goal with
          | [ _ : _ |- context [ if ?X then _ else _ ] ] => destruct X
          end;
        try assumption;
        simpl in Hsl |- *.

      - rewrite (state_leb_diff opc st st' Hed) in Hsl.
        apply state_leb_diff in Hed.
        destruct (stage_beq (nstg _) _) eqn:Hsb.
        + now rewrite (stage_beq_nstg_nstg_eq st st').
        + now apply leb_far_remains.

      - rewrite (state_leb_diff opc st st' Hed) in Hsl.
        apply state_leb_diff in Hed.
        destruct (stage_beq (nstg _) _).
        + reflexivity.
        + now apply (leb_remains st st').

      - now apply right_progresses_state_leb.
      - now apply right_progresses_state_leb.
    Qed.

    Lemma moved_lat_is_monotonic :
      forall n n', e = (opc, (st, n)) -> e' = (opc, (st', n')) ->
              state_leb (st, n) (st', n') = true -> e <> e' ->
              compare_two (List.nth_error tc i) (List.nth_error tc' i) = true.
    Proof.
      intros n n' He He' Hsl Hed.
      clear HfreePersists Hlsbt.
      specialize (HvalidLat e').
      simpl in Hsl.

      destruct n';
        [now apply (moved_lat_left_is_monotonic n) |];
        work_on_e Hed;
        destruct n.

      - pose proof (state_leb_n0 _ _ _ Hsl) as Hst.
        rewrite Hst in Hsl.
        unfold compare_two, state_leb.

        destruct (_ && _).
        + destruct (stage_beq (nstg _) _) eqn:Hsb.
          * rewrite stage_beq_iff_eq in Hsb.
            rewrite Hsb, Nat.leb_le.
            lia.
          * now apply leb_remains.
        + now rewrite Hst.

      - exact Hsl.
    Qed.

    Theorem is_monotonic :
      forall n n', e = (opc, (st, n)) -> e' = (opc, (st', n')) ->
              state_leb (st, n) (st', n') = true ->
              compare_two (List.nth_error tc i) (List.nth_error tc' i) = true.
    Proof.
      intros n n' He He' Hsl.

      (* e = e' or e <> e'. *)
      compare e e'.
      - (* If e = e', we have a lemma for this. *)
        now apply (unmoved_is_monotonic n).
      - (* If e <> e', we have a lemma for this. *)
        now apply (moved_lat_is_monotonic n n').

      (* state and opcode are comparable. *)
      - repeat decide equality.
      - repeat decide equality.
    Qed.
  End simple_monotonicity.

  Let tc_and_tc' :=
        tc_tc'_length_eq (cycle_elt (next_id t) sbState t (memNotFree t)) (cycle_elt (next_id t') sbState' t' (memNotFree t'))
          t t' tc tc' Hleb Htc Htc'.

  Variable e e' : instr_kind.
  Variable n n' : nat.

  Hypothesis Ht : t = tpre ++ e :: tpost.
  Hypothesis Ht' : t' = tpre' ++ e' :: tpost'.
  Hypothesis Hi : forall i, i = List.length tpre.
  Hypothesis Hi' : forall i, i = List.length tpre'.

  Hypothesis He : e = (opc, (st, n)).
  Hypothesis He' : e' = (opc, (st', n')).
  Hypothesis Hsl : state_leb (st, n) (st', n') = true.

  Let is_monotonic' ni :=
        is_monotonic e e' ni Ht Ht' (Hi ni) (Hi' ni) n n' He He' Hsl.

  Let cycled_instrs_are_leb' :=
        cycled_instrs_are_leb d tc tc' is_monotonic' tc_and_tc'.

  Theorem is_fully_monotonic : pipeline_leb tc tc' = true.
  Proof.
    now apply is_pipeline_fully_monotonic.
  Qed.
End monotonicity.
