Require Import Bool List Nat PeanoNat.
Import ListNotations.

Module Type Sig.
  Parameter opcode : Set.

  Parameter stage : Set.
  Axiom first_stage : stage.
  Axiom stage_beq : stage -> stage -> bool.
  Axiom stage_beq_eq_true : forall s, stage_beq s s = true.
  Axiom stage_beq_iff_eq : forall s s', stage_beq s s' = true <-> s = s'.

  Axiom leb : stage -> stage -> bool.
  Axiom idx : opcode -> nat.
End Sig.

Module Pipeline (Def : Sig).
  Import Def.
  Export Def.

  Definition state := (stage * nat)%type.
  Definition instr_kind := (opcode * state)%type.
  Definition trace_kind := list instr_kind.

  Definition is_in_stage st (instr : instr_kind) :=
    match instr with
    | (_, (st', _)) => stage_beq st st'
    end.

  Definition get_instr_in_stage trace st := List.find (is_in_stage st) trace.

  Definition state_leb st1 st2 :=
    match st1, st2 with
    | (s1, n1), (s2, n2) =>
        if stage_beq s1 s2 then
          Nat.leb n2 n1
        else
          leb s1 s2
    end.

  Fixpoint min_element l :=
    match l with
    | [] => 0
    | x :: [] => x
    | x :: l => Nat.min x (min_element l)
    end.

  Definition next_id (trace : trace_kind) :=
    let filtered := List.filter (fun (elt : instr_kind) =>
                                   match elt with
                                   | (_, (st, _)) => stage_beq st first_stage
                                   end) trace in
    let mapped := List.map (fun (elt : instr_kind) =>
                              match elt with
                              | (opc, _) => idx opc
                              end) filtered in
    min_element mapped.

  Lemma state_leb_eq_true : forall st, state_leb st st = true.
  Proof.
    intro st.
    destruct st.
    simpl.
    now rewrite stage_beq_eq_true, Nat.leb_le.
  Qed.

  Definition compare_two (e1 e2 : option instr_kind) :=
    match e1, e2 with
    | None, _
    | _, None => false
    | Some (_, st1), Some (_, st2) => state_leb st1 st2
    end.

  Lemma compare_two_eq_true : forall e, e <> None -> compare_two e e = true.
  Proof.
    intros e Hn.
    destruct e as [[o s] |].
    - (* e = Some (o, s) *)
      simpl.
      now rewrite state_leb_eq_true.
    - (* None <> None *)
      contradiction.
  Qed.

  Definition instrs_leb (instrs : (instr_kind * instr_kind)) :=
    match instrs with
    | ((_, st1), (_, st2)) => state_leb st1 st2
    end.

  Definition pipeline_leb p1 p2 :=
    let comb := List.combine p1 p2 in
    (List.length p1 =? List.length p2) && List.forallb instrs_leb comb.

  Section comparison.
    Variable t t' : trace_kind.
    Variable i : nat.
    Variable d : instr_kind.

    Hypothesis Hct : compare_two (List.nth_error t i) (List.nth_error t' i) = true.

    Lemma compare_two_true_means_exists :
      (exists e, List.nth_error t i = Some e)
      /\ (exists e, List.nth_error t' i = Some e).
      split.
      - (* First case. *)
        destruct (nth_error t _).
        + now exists i0. (* Some i0 = Some e -> i0 = e. *)
        + discriminate. (* compare_two None ... = true is a contradiction. *)

      - (* Second case. *)
        destruct (nth_error t' _).
        + now exists i0. (* Some i0 = Some e -> i0 = e. *)
        + (* compare_two ... None = true is a contradiction, with extra steps. *)
          destruct (nth_error t _).
          * simpl in Hct.
            destruct i0.
            discriminate.
          * discriminate.
    Qed.

    Lemma compare_two_true_means_instrs_leb :
      instrs_leb (List.nth i t d, List.nth i t' d) = true.
    Proof.
      destruct compare_two_true_means_exists as [Hec Hec'].
      destruct Hec as [ec Hnth], Hec' as [ec' Hnth'].
      rewrite Hnth, Hnth' in Hct.

      now rewrite (nth_error_nth t i d Hnth), (nth_error_nth t' i d Hnth').
    Qed.
  End comparison.

  Lemma tc_tc'_length_eq :
    forall (f f' : instr_kind -> instr_kind) (t t' tc tc' : trace_kind),
      pipeline_leb t t' = true ->
      tc = map f t -> tc' = map f' t' -> length tc = length tc'.
  Proof.
    intros f f' t t' tc tc' Hleb Htc Htc'.

    (* length t = length t'. *)
    (* length (map ... t) = length t. *)
    rewrite Htc, Htc'.
    repeat rewrite map_length.
    unfold pipeline_leb in Hleb.
    now rewrite andb_true_iff, Nat.eqb_eq in Hleb.
  Qed.

  Lemma cycled_instrs_are_leb :
    forall (d : instr_kind) (tc tc' : trace_kind),
      (forall i, compare_two (nth_error tc i) (nth_error tc' i) = true) ->
      List.length tc = List.length tc' ->
      forall instrs, In instrs (combine tc tc') -> instrs_leb instrs = true.
  Proof.
    (* Since instrs is in (combine tc tc'), there is a
     * i' < length (combine tc tc') and
     * nth i' (combine tc tc') (d, d)) = instrs. *)
    intros d tc tc' Hct Hlength instrs HIn.
    destruct (In_nth (combine tc tc') instrs (d, d) HIn) as [i [Hil HInth]].

    (* since nth i (combine tc tc') = instrs,
     * instrs = (nth i tc d, nth i tc' d). *)
    rewrite combine_nth in HInth.
    - (* instrs_leb instrs = true means
       * compare_two (nth_error tc i) (nth_error tc' i) = true.
       * We have a lemma for this. *)
      (* Then, compare_two (nth_error tc i) (nth_error tc' i) = true.
       * We have a theorem for this. *)
      rewrite <- HInth.
      apply (compare_two_true_means_instrs_leb tc tc' i d), (Hct i).
    - exact Hlength.
  Qed.

  Lemma is_pipeline_fully_monotonic :
    forall (tc tc' : trace_kind),
      List.length tc = List.length tc' ->
      (forall instrs, In instrs (combine tc tc') -> instrs_leb instrs = true) ->
      pipeline_leb tc tc' = true.
  Proof.
    intros tc tc' Hlength Hleb.

    unfold pipeline_leb.
    rewrite andb_true_iff.
    now split; [
        (* First, length tc = length tc'.  We have a hypothesis for this. *)
        rewrite Nat.eqb_eq |
        (* Now, forallb instrs_leb (combine tc tc') = true. *)
        (* Transform the goal into
         * forall instrs, In instrs (combine ...) -> instrs_leb instrs = true.
         * Directly apply Hleb. *)
        rewrite forallb_forall
      ].
  Qed.
End Pipeline.

Variant sbStateT :=
  | Empty
  | NotEmpty
  | Full.

Scheme Equality for sbStateT.
